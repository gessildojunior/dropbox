<?php
$CONFIG = array (
  'htaccess.RewriteBase' => '/',
  'memcache.local' => '\\OC\\Memcache\\APCu',
  'apps_paths' => 
  array (
    0 => 
    array (
      'path' => '/var/www/html/apps',
      'url' => '/apps',
      'writable' => false,
    ),
    1 => 
    array (
      'path' => '/var/www/html/custom_apps',
      'url' => '/custom_apps',
      'writable' => true,
    ),
  ),
  'instanceid' => 'ocgiijyfenir',
  'passwordsalt' => 'LLfFW87bzdVRsC3kprxgcI4qrfeUiM',
  'secret' => '8Y5lTswgX8HMVCM2jAun6KxFScnpLjqwMtoJhdEvISe4Z4uD',
  'trusted_domains' => 
  array (
    0 => '150.161.234.81:81',
  ),
  'datadirectory' => '/var/www/html/data',
  'dbtype' => 'sqlite3',
  'version' => '24.0.2.1',
  'overwrite.cli.url' => 'http://150.161.234.81:81',
  'installed' => true,
);
