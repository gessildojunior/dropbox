<?php 
$OC_Version = array(24,0,2,1);
$OC_VersionString = '24.0.2';
$OC_Edition = '';
$OC_Channel = 'stable';
$OC_VersionCanBeUpgradedFrom = array (
  'nextcloud' => 
  array (
    '23.0' => true,
    '24.0' => true,
  ),
  'owncloud' => 
  array (
    '10.5' => true,
  ),
);
$OC_Build = '2022-06-20T15:28:09+00:00 2764c381a054ca8284975e70037e96411d2ce8f8';
$vendor = 'nextcloud';
